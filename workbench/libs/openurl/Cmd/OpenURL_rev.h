#define PRG         "OpenURL"
#define VERSION     7
#define REVISION    2
#define VRSTRING    "7.2"
#define DATE        "09.02.2006"
#define VERS        "OpenURL "VRSTRING
#define PRGNAME     VERS" ("DATE")"
#define VSTRING     PRGNAME"\r\n"
#define VERSTAG     "\0$VER: "PRGNAME
