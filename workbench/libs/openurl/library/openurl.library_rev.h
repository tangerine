#define PRG         "openurl.library"
#define UPRG        "OPENURL.LIBRARY"
#define VERSION     7
#define REVISION    2
#define VRSTRING    "7.2"
#define DATE        "1.12.2005"
#define VERS        "openurl.library 7.2"
#define VSTRING     "$VER: openurl.library 7.2 (1.12.2005)\r\n"
#define VERSTAG     "\0$VER: openurl.library 7.2 (1.12.2005)"
#define PRGNAME     "openurl.library 7.2 (1.12.2005)"
