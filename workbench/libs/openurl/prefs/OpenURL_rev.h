#define PRG         "OpenURL"
#define UPRG        "OPENURL"
#define VERSION     7
#define REVISION    2
#define VRSTRING    "7.2"
#define DATE        "06.11.2007"
#define VERS        "OpenURL 7.2"
#define VSTRING     "$VER: OpenURL 7.2 (06.11.2007)\r\n"
#define VERSTAG     "\0$VER: OpenURL 7.2 (06.11.2007)"
#define PRGNAME     "OpenURL 7.2 (06.11.2007)"
