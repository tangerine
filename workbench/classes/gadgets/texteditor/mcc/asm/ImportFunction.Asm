
; a0 = text
; a2 = poolhandle
; a3 = Hook
; d0 = sizeof(line_node)
; d4 = ImportWrap

	IncDir	Includes:
	Include	LVO/Exec_Lib.I
	Include	Exec/Types.I


	STRUCTURE	HookMsg,0
		APTR	HM_Data
		APTR	HM_LineNode
		APTR	HM_PoolHandle
		LONG	HM_ImportWrap
		LABEL	SizeOf_HookMsg

	STRUCTURE	LineNode,0
		APTR	LN_Next
		APTR	LN_Prev
		APTR	LN_Contents
		LONG	LN_Length
		APTR	LN_Styles
		APTR	LN_Colors
		BOOL	LN_Color
		WORD	LN_Flow
		WORD	LN_Separator

	XDef	_ImportText

_ImportText:	movem.l	d1-a6,-(sp)
	clr.l	FirstLine
	clr.w	Flow

	move.l	d4,-(sp)
	move.l	a2,-(sp)
	subq.w	#4,sp
	move.l	a0,-(sp)

	move.l	d0,d2
	sub.l	a4,a4
	lea.l	(a2),a5

.NextLine	move.l	d2,d0
	lea.l	(a5),a0
	move.l	$4.w,a6
	jsr	_LVOAllocPooled(a6)
	move.l	d0,a1
	tst.l	d0
	beq	.IsNotEmpty

	clr.l	LN_Next(a1)
	cmp.l	#0,a4
	beq.b	.FirstTime
	move.l	a1,LN_Next(a4)
	move.l	a4,LN_Prev(a1)
	bra.b	.skip

.FirstTime	clr.l	LN_Prev(a1)
	move.l	a1,FirstLine

.skip	clr.w	LN_Separator(a1)
	move.w	Flow(pc),LN_Flow(a1)
	clr.w	LN_Color(a1)
	clr.l	LN_Colors(a1)
	clr.l	LN_Styles(a1)
	clr.l	LN_Length(a1)
	clr.l	LN_Contents(a1)
	move.l	a1,a4

	addq.w	#8,a1
	move.l	a1,HM_LineNode(sp)

	lea.l	(sp),a1
	movem.l	d2/a3-a5,-(sp)
	lea.l	(a3),a0
	move.l	8(a0),a6
	jsr	(a6)
	movem.l	(sp)+,d2/a3-a5
	move.w	LN_Flow(a4),Flow
	move.l	d0,HM_Data(sp)
	bne.b	.NextLine

	tst.l	LN_Contents(a4)
	bne	.IsNotEmpty

	lea.l	(a5),a0
	lea.l	(a4),a1
	move.l	LN_Prev(a4),a4
	move.l	$4.w,a6
	jsr	_LVOFreePooled(a6)

.IsNotEmpty	lea.l	SizeOf_HookMsg(sp),sp
	movem.l	(sp)+,d1-a6
	move.l	FirstLine(pc),d0
	rts

FirstLine:	dc.l	0
Flow:	dc.w	0


                                          � !