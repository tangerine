/*
**
** $VER: HotkeyString_mcc.h V11.0 (28-Sep-97)
** Copyright � 1997 Allan Odgaard. All rights reserved.
**
*/

#ifndef   HOTKEYSTRING_MCC_H
#define   HOTKEYSTRING_MCC_H

#ifndef   EXEC_TYPES_H
#include  <exec/types.h>
#endif

#define   MUIC_HotkeyString     "HotkeyString.mcc"
#define   HotkeyStringObject    MUI_NewObject(MUIC_HotkeyString

#define MUIA_HotkeyString_Snoop 0xad001000

#endif /* HOTKEYSTRING_MCC_H */
